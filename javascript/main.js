const salaryDisplay = document.getElementById("salaryDisplay");
const bankBalanceDisplay = document.getElementById("bankBalanceDisplay");
const laptopSelect = document.getElementById("laptopSelect");
const laptopSpecs = document.getElementById("laptopSpecs");
const laptopImage = document.getElementById("laptopImage");
const laptopTitle = document.getElementById("laptopTitle");
const laptopDescription = document.getElementById("laptopDescription");
const laptopPrice = document.getElementById("laptopPrice");

// hardcoded laptop resources
const laptops = [{
    name: "Crapbook x2000",
    features: ["It has a screen", "Hardly sticky at all", "Has not been wiped"],
    price: "19 00",
    description: "It is the world renown crapbook, crappier than ever before.",
    image: "http://placekitten.com/150/151"
  },
  {
    name: "Hp avantgarde 360",
    features: ["Black finish", "Got some cords", "some other description"],
    price: "13 00",
    description: "Hp is the best choice for old folks. nuff said",
    image: "http://placekitten.com/151/150"
  },
  {
    name: "Samsung galaxy 29",
    features: ["Small", "efficent", "Samsung"],
    price: "10 00",
    description: "Wait aint this a phone?.",
    image: "http://placekitten.com/151/151"
  },
  {
    name: "super duper gaming computer",
    features: [
      "1313fd razer graphic pixels", "trackball merged into screen",
      "slightly used"
    ],
    price: "9 000",
    description: "Angry bois gonna be angry bois.",
    image: "http://placekitten.com/153/152"
  },
];

// the bank where our assets will be saved and other "banky stuff"
class Bank {
  loanTaken = false;
  balanceInKr = 0;
  /*
  * If the user want to take a loan and it does not exceed more than double it is rejected,
  * but exactly double is fine as stated in the requierments.
  */
  takeLoan =
    () => {
      if (this.loanTaken) {
        prompt(
          "You already have a loan, go buy a compute before you loan again");
        return;
      }
      const requestedLoanAmount =
        prompt("Enter the loan amount you want to take");

      if (this.balanceInKr * 2 < requestedLoanAmount) {
        confirm("Amount requested is to big");
        return;
      }

      this.balanceInKr += parseInt(requestedLoanAmount);

      this.loanTaken = true;
    }

  // transfer of money, specifically from the salary
  recieveAssets = assets => {
    this.balanceInKr += assets;
  }

  getBalance = () => this.balanceInKr;

  setBankbalance = newBalance => {
    this.balanceInKr = newBalance;
  }
}

// work related class which handels "worky stuff"
class Work {
  salaryInKr = 0;

  recieveDailyPayment = () => {
    this.salaryInKr += 100;
  }

  transferToBank = () => {
    const assets = this.salaryInKr;
    this.salaryInKr = 0;
    return assets;
  }
}
/*
* contoller to knit everything together and give orders to the other classes,
* the other classes should not have contact unless it is through the controller.
*/
class Controller {
  constructor(bank, work, view) {
    this.bank = bank;
    this.work = work;
    this.view = view;
  }

  // get paid from work
  recievePayment =
    () => {
      work.recieveDailyPayment();
      view.updateSalary(work.salaryInKr)
    }


  transferSalaryToBank =
    () => {
      this.bank.recieveAssets(work.transferToBank());
      this.view.updateSalary(0);
      this.view.updateBankBalance(bank.balanceInKr);
    }
  getALoan =
    () => {
      this.bank.takeLoan();
      this.view.updateBankBalance(bank.balanceInKr);
    }

  setLaptop = () => {
    view.setLaptop();
  }

  buyLaptop = () => {

    if (this.view.selectedLaptop) {

      let bankBalance = this.bank.getBalance();
      let laptopPrice = parseInt(this.view.getSelectedLaptopPrice().replace(/ /g, ''));

      if (laptopPrice > bankBalance) {
        confirm("You do not have enough money to buy this laptop")
      } else {
        bankBalance -= laptopPrice;
        this.bank.setBankbalance(bankBalance);
        this.bank.loanTaken = false;
        this.view.updateBankBalance(bankBalance);
        confirm("Congratulations on your new laptop");
      }
    } else {
      confirm("you have not selected a laptop");
    }
  }
}

// class to controll the visuals on the site
class View {
  constructor(salaryDisplay, bankBalanceDisplay, laptops, laptopSelect,
    laptopSpecs, laptopImage, laptopTitle, laptopDescription,
    laptopPrice) {

    this.selectedLaptop = null;
    this.salaryDisplay = salaryDisplay;
    this.bankBalanceDisplay = bankBalanceDisplay;
    this.laptops = laptops;
    this.laptopSelect = laptopSelect;
    this.laptopSpecs = laptopSpecs;
    this.laptopImage = laptopImage;
    this.laptopTitle = laptopTitle;
    this.laptopDescription = laptopDescription;
    this.laptopPrice = laptopPrice;

    this.laptops.forEach((laptop) => {
      let option = document.createElement("option");
      option.text = laptop.name;
      option.value = laptop.name;
      this.laptopSelect.appendChild(option);
    });
  }

  updateSalary = (salary) => {
    this.salaryDisplay.innerText = `${salary} kr`;
  }

  updateBankBalance =
    (balance) => {
      this.bankBalanceDisplay.innerText = `${balance} kr`;
    }

  getSelectedLaptopPrice = () => this.selectedLaptop.price;

  setLaptop = () => {
    // clear UI before appending new children,
    this.laptopSpecs.innerHTML = "";
    let features = document.createElement("li");
    features.innerHTML = `<b>Features:</b>`;
    this.laptopSpecs.appendChild(features);

    this.laptops.forEach((laptop) => {
      if (laptop.name === laptopSelect.value) {

        // TODO break out to own function
        laptop.features.forEach((spec) => {
          const li = document.createElement("li");
          li.innerHTML = spec;
          this.laptopSpecs.appendChild(li);
        });
        this.selectedLaptop = laptop;
        this.laptopImage.src = laptop.image;
        this.laptopTitle.innerHTML = laptop.name;
        this.laptopDescription.innerHTML = laptop.description;
        this.laptopPrice.innerHTML = laptop.price + " kr";
      }
    });
  }
}

const bank = new Bank();
const work = new Work();
const view = new View(salaryDisplay, bankBalanceDisplay, laptops, laptopSelect,
  laptopSpecs, laptopImage, laptopTitle, laptopDescription,
  laptopPrice);
const controller = new Controller(bank, work, view);
